/*





*/
module compare_larger
( 
   clk               , // system clock
   dataa             ,
   datab             ,
   q                           
);

input  wire         clk;
input  wire [15 :0] dataa;
input  wire [15 :0] datab;

output reg  [15 :0] q;

always @(posedge clk)
begin

   q <= (dataa > datab ) ? dataa : datab;
   
end

endmodule