/*





*/
module ped_range_finder
( 
   clk               , // system clock
   reset             ,
   adc_in            ,
   out_min           ,
   out_max           
);

input  wire         clk;
input  wire         reset;
input  wire [13 :0] adc_in;
output reg  [15 :0] out_min;
output reg  [15 :0] out_max;

reg                 pre_in;
reg                 en_counter;

reg        [5 :0]   cnt = 6'b11_1111;

always @(posedge clk)
begin

   /// counter is increased if it does not hit the maximum 63.
   cnt <= (cnt < 6'h3F ) ? (cnt + 1) : cnt;

   /// If the counter is not 63 yet, find the max and min
   out_max <= ( cnt < 6'h3F && adc_in > out_max[13:0] ) ? adc_in : out_max;
   out_min <= ( cnt < 6'h3F && adc_in < out_min[13:0] ) ? adc_in : out_min;

   /// reset counter 
   if( reset==1'b1 ) begin
      cnt <= 0;
      out_max <= 16'h0000;
      out_min <= 16'hFFFF;
   end
    
end

endmodule