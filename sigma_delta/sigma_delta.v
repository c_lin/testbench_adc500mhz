/*





*/
module compare_smaller
( 
   clk               , // system clock
   dataa             ,
   datab             ,
   q                           
);

input  wire         clk;
input  wire [11 :0] dataa;
input  wire [11 :0] datab;

output reg  [15 :0] q;

always @(posedge clk)
begin

   /// counter is increased if it does not hit the maximum 63.
   q[11: 0] <= (dataa < datab ) ? dataa : datab;
   q[15:12] <= 4'b0;
   
end

endmodule